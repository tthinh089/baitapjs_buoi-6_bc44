handleResult = () => {
  var content = "";
  for (i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      content += `<h3 class="bg bg-primary p-2 m-0"> Div chẵn ${i} </h3>`;
    } else {
      content += `<h3 class="bg bg-danger p-2 m-0"> Div lẻ ${i} </h3>`;
    }
  }
  document.querySelector("#result").innerHTML = content;
};
